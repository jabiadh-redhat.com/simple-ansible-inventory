#!/usr/bin/env bash

cat << EOF
{
    "_meta": {
        "hostvars": {
            "localhost": {
                "ansible_connection": "local"
            },
            "satellite.localdomain": {
                "ansible_user": "root"
            }
        }
    },
    "all": {
        "children": [
            "ungrouped",
            "vms"
        ]
    },
    "ungrouped": {
        "hosts": [
            "localhost"
        ]
    },
    "vms": {
        "hosts": [
            "satellite.localdomain"
        ]
    }
}
EOF
